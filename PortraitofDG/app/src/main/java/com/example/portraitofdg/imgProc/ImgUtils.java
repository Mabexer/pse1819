package com.example.portraitofdg.imgProc;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;

import java.io.ByteArrayOutputStream;

public class ImgUtils {

    /**
     * Resize a specified bitmap to the specified width and height
     * @param bitmap    Bitmap to resize
     * @param width     width desired dimension
     * @param height    height desired dimension
     * @return Resized bitmap
     */
    public static Bitmap resizeBitmap(Bitmap bitmap, int width, int height) {

        // Parameters check
        if (bitmap == null) {
            Log.w("ImgUtils_resizeBitmap", "Bitmap is null");
            return null;
        }
        if (width <= 0 || height <= 0) {
            Log.w("ImgUtils_resizeBitmap", "width and height must be strictly positive");
            return null;
        }

        // Create empty output bitmap
        Bitmap resized = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        // Create a canvas which will draw on the output bitmap
        final Canvas canvas = new Canvas(resized);
        // Scale and translates to fill the output bitmap
        canvas.drawBitmap(
                bitmap,
                new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new Rect(0, 0, width, height),
                null);

        return resized;
    }

    /**
     * Draws the specified mask on a bitmap
     * @param mask      Mask to draw
     * @param color     Color used to represent a non-background pixel
     * @return Bitmap of the mask
     */
    public static Bitmap drawMask(int[][] mask, int color) {

        Bitmap output = Bitmap.createBitmap(mask.length, mask.length, Bitmap.Config.ARGB_8888);

        for (int y = 0; y < mask.length; y++) {
            for (int x = 0; x < mask.length; x++) {
                // Background is transparent the rest is colored
                if (mask[x][y] > 0)
                    output.setPixel(x, y, color);
                else
                    output.setPixel(x, y, Color.TRANSPARENT);
            }
        }
        return output;
    }

    /**
     * Builds an array of bytes from the specified bitmap
     * @param bmp   A Bitmap object
     * @return      An array of bytes representing the object
     */
    public static byte[] bitmapToByteArray(Bitmap bmp){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bmp.recycle();
        return byteArray;
    }

}
