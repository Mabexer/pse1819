package com.example.portraitofdg.tflite;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.util.Log;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class DeepLabTFLite {

    private final static String MODEL_PATH = "deeplabv3_257_mv_gpu.tflite";

    // Image mean
    private static final float IMAGE_MEAN = 128.0f;
    // Image standard deviation
    private static final float IMAGE_STD = 128.0f;
    // Number of pixels per axis
    private final static int INPUT_SIZE = 257;
    // Number of classes outputted by the neural network
    private final static int NUM_CLASSES = 21;
    // ID of the person class for the neural network
    private final static int PERSON_CLASS = 15;
    // Number of color channels
    private final static int COLOR_CHANNELS = 3;
    // Number of byters per pixel
    private final static int BYTES_PER_POINT = 4;
    // Number of input images at a time
    private final static int BATCH_SIZE = 1;

    // Model buffer
    private MappedByteBuffer mModelBuffer;

    // Input and output buffers
    private ByteBuffer mInput;
    private ByteBuffer mOutput;

    // Output mask
    private int[][] mSegmentBits;

    /**
     * Initialize all the parameters for the execution of the neural network
     * @param context   The application context
     * @return true if the initialization was successfull
     */
    public boolean initialize(Context context) {

        // Check parameter
        if (context == null) return false;

        try {
            mModelBuffer = loadModelFile(context);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        // Check if loaded successfully
        if (mModelBuffer == null) return false;

        // The image will be represented by a 1D array with three bytes per channel (RGB)
        // Uses a direct buffer, which avoids intermediate buffers at the cost of a higher allocation cost
        mInput = ByteBuffer.allocateDirect(
                BATCH_SIZE * INPUT_SIZE * INPUT_SIZE * COLOR_CHANNELS * BYTES_PER_POINT);
        // To ensure the bits are stored in the native order of the device
        mInput.order(ByteOrder.nativeOrder());

        // Allocate the output buffer the same way as the input buffer
        mOutput = ByteBuffer.allocateDirect(
                BATCH_SIZE * INPUT_SIZE * INPUT_SIZE * NUM_CLASSES * BYTES_PER_POINT);
        mOutput.order(ByteOrder.nativeOrder());

        // Allocate the mask
        mSegmentBits = new int[INPUT_SIZE][INPUT_SIZE];

        return true;
    }

    /**
     * Resize the input bitmap in order to adapt it to the constraints of the NN. Then it executes the NN.
     * @param bitmap Bitmap of the input image
     * @return  The output mask
     */
    public int[][] segment(Bitmap bitmap) {

        if (mModelBuffer == null) {
            Log.w("DeepLabTFLite_segment", "Model is not initialized.");
            return null;
        }
        if (bitmap == null) {
            Log.w("DeepLabTFLite_segment", "Bitmap is null.");
            return null;
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width != INPUT_SIZE || height != INPUT_SIZE) {
            Log.w("DeepLabTFLite_segment", "Input size mismatch.");
            return null;
        }

        // Rewind the buffers to be sure
        mInput.rewind();
        mOutput.rewind();

        int[] intValues = new int[width * height];
        // Return pixels as packed ints
        bitmap.getPixels(intValues, 0, width, 0, 0, width, height);

        int pixel = 0;
        // For each pixel
        for (int i = 0; i < INPUT_SIZE; i++) {
            for (int j = 0; j < INPUT_SIZE; j++) {
                if (pixel >= intValues.length) break;

                final int val = intValues[pixel++];
                // Unpack the values by applying a shift followed by a mask of the least significant bits
                // then IMAGE_MEAN is subtracted and divided by IMAGE_STD
                // the result is a distribution of pixels which is similar to a Gaussian with
                // zero mean and a unitary standard deviation

                // The alpha channel (val >> 24) is ignored.
                // Red channel
                mInput.putFloat((((val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                // Green channel
                mInput.putFloat((((val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                // Blue channel
                mInput.putFloat(((val & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
            }
        }

        // Intialize the interpreter with the allocated model and run it
        Interpreter interpreter = new Interpreter(mModelBuffer, null);

        final long start = System.currentTimeMillis();
        Log.i("DeepLabTFLite_segment", "Interpreter starting: " + start);

        interpreter.run(mInput, mOutput);

        final long end = System.currentTimeMillis();
        Log.i("DeepLabTFLite_segment", "Interpreter ending: " + end);
        Log.i("DeepLabTFLite_segment", "Elapsed: " + (end - start) + " ms");

        float maxVal = 0;
        float val = 0;

        // For each pixel
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // For each class
                for (int c = 0; c < NUM_CLASSES; c++) {
                    // Get the value at index (VERTICAL_OFFSET + HORIZONTAL_OFFSET + CLASS_OFFSET) * BYTES_PER_POINT
                    val = mOutput.getFloat((y*width*NUM_CLASSES + x*NUM_CLASSES + c) * BYTES_PER_POINT);
                    // Update the label if the current class has a higher value
                    if (c == 0 || val > maxVal) {
                        // This also reinitializes maxVal
                        maxVal = val;
                        mSegmentBits[x][y] = c;
                    }
                }
            }
        }

        return mSegmentBits;
    }

    /***
     * Opens the model using an input stream and memory map it to load
     * @param context
     * @return Memory mapping of the model
     * @throws IOException The model was not found
     */
    private MappedByteBuffer loadModelFile(Context context) throws IOException {

        // Check parameters
        if (context == null) return null;

        // Open the model using an input stream and memory map it to load
        AssetFileDescriptor fileDescriptor = context.getAssets().openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    /***
     * Returns the input size of the neural network
     * @return the input size of the neural network
     */
    public static int getInputSize() {
        return INPUT_SIZE;
    }

    /***
     * Returns the "person" id of the neural network
     * @return the "person" id of the neural network
     */
    public static int getPersonClass() {
        return PERSON_CLASS;
    }

    /**
     * Filter the mask by the given label
     * @param mask      The mask containing all the labels
     * @param label     The label to extract
     * @param value     The value to assign to the pixel corresponding to that label
     * @return          A new mask filtered by the input label
     */
    public static int[][] getSingleLabelMask(int[][] mask, int label, int value) {

        // Check parameters
        if (mask == null) return null;
        if (label < 0 || label >= NUM_CLASSES) return null;

        int[][] output = new int[mask.length][mask.length];

        // Apply the value to the elements of the label, zero otherwise
        for (int y = 0; y < mask.length; y++) {
            for (int x = 0; x < mask.length; x++) {
                if (mask[x][y] == label)
                    output[x][y] = value;
                else
                    output[x][y] = 0;
            }
        }

        return output;
    }
}
