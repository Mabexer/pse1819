package com.example.portraitofdg;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

/**
 * MainActivity is the entry point activity of this app.
 * It displays a simple button to open the gallery allowing the user to pick a photo.
 * In order to work, the photo must contain at least one human subject.
 * By selecting the check box below the button the user can decide to use a refinement algorithm
 * for the segmentation of the photo. The specific algorithm to use is selectable
 * through the spinner under the checkbox.
 *
 * @author Filippo Checchinato, Davide De Pieri
 *
 */
public class MainActivity extends AppCompatActivity {

    final static int GALLERY_REQUEST_CODE = 0;
    // Spinner and its selected element
    private Spinner mSpinner;
    private int mSelectedItem;
    // Checkbox and its value
    private CheckBox mCheckBox;
    private boolean mUseAlg;

    public static String EXTRA_USE_ALG = "EXTRA_USE_ALG";
    public static String EXTRA_SELECTED_ITEM = "EXTRA_SELECTED_ITEM";

    /**
     * Initialize the parameters at their initial state. By default the checkbox is not selected.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Check Box
        mSpinner = findViewById(R.id.spinner);
        mUseAlg = false;

        // Initialize Spinner
        mCheckBox = findViewById(R.id.checkBox);
        mCheckBox.setSelected(false);
        ArrayList<String> items = new ArrayList<>();
        items.add("LCS");
        items.add("SLIC");
        mSelectedItem = 0;
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinner.setAdapter(arrayAdapter);
        // The spinner si disabled by default
        mSpinner.setEnabled(false);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedItem = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    /**
     * Listener of the pick button. Sends an implicit intent for an image app
     * in order to choose a photo stored in the phone memory.
     * @param view
     */
    public void pickFromGallery(View view) {

        // Create an Intent to pick an item from data and returning the selection
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Specify the type of the item, an image
        intent.setType("image/*");
        // Specify the MIME type, only jpeg and png are allowed
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    /**
     * Called at the end of the startActivityForResult(...) method.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Check status
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    // Get URI of the image
                    Uri selectedImage = data.getData();
                    // Start segmentation activity with the selected parameters
                    Intent intent = new Intent(this, SegmentationActivity.class);
                    intent.setData(selectedImage);
                    intent.putExtra(EXTRA_USE_ALG, mUseAlg);
                    intent.putExtra(EXTRA_SELECTED_ITEM, mSelectedItem);

                    startActivity(intent);
                    break;
            }

    }

    /**
     * Listener of the checkbox. Sets the parameter mUseAlg to true or false.
     * @param view
     */
    public void onCheckBoxClicked(View view) {
        // The spinner is activated only if the box is ticked
        mUseAlg = !mUseAlg;
        mSpinner.setEnabled(!mSpinner.isEnabled());
    }

    /**
     * Reset the views to their previous state
     */
    @Override
    protected void onResume() {
        super.onResume();
        mCheckBox.setSelected(mUseAlg);
        mSpinner.setEnabled(mUseAlg);
        mSpinner.setSelection(mSelectedItem);
    }
}
