package com.example.portraitofdg;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.portraitofdg.imgProc.ImgUtils;
import com.example.portraitofdg.tflite.DeepLabTFLite;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Algorithm;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.ximgproc.SuperpixelLSC;
import org.opencv.ximgproc.SuperpixelSLIC;
import org.opencv.ximgproc.Ximgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Selects any human subject found in the photo by the neural network DeepLabV3+ as a mask
 * containing the segmentation. If the checkbox in MainActivity activity was selected a refinement
 * is applyed to the mask in order to obtain a more accurate result.
 * Finally a blur is applied to the background.
 * By using the three sliders the user can adjust the amount of blur in the background, the gradient
 * of the blur between the background and the foreground, erosion or dilation can also be applied
 * to the mask for a finer tuning of the shape.
 */
public class SegmentationActivity extends AppCompatActivity {

    // Uri of the input image
    private Uri mInputImage;
    // Bitmap and Mat of the input image
    private Bitmap mInputBitmap;
    private Mat mInputMat;

    // Bitmap of the neural network mask
    private Bitmap mDeepMaskBitmap;
    // To control the blurring
    private SeekBar mBlurBar;
    // To control the gradient of the blur
    private SeekBar mBlurGraduationBar;
    private double mFinalBlurGraduation;
    // To control the erosion
    private SeekBar mErosionBar;
    // Bitmap and Mat of the upscaled mask from the neural network and segmentation algorithms
    private Bitmap mUpscaledMask;
    private Mat mUpscaledMaskMat;
    // Bitmap of the mask with erosion/dilation components
    private Bitmap mFinalMask;
    private Mat mFinalMaskMat;
    // Value of the morphology operator for the final mask
    private int mFinalMorphValue;
    // Bitmap of the blurred image
    private int mFinalBlurredValue;
    // Bitmap of the mOutputBitmap
    private Bitmap mOutputBitmap = null;

    // To show that some operations are in action
    private ProgressBar mProgressBar;
    // To display the image on the screen
    private ImageView mPreviewImage;

    // Values for operation selection
    private static final int sPipelineAction = 0;
    private static final int sBlurAction = 1;
    private static final int sMorphologyAction = 2;
    private static final int sBlurringGraduationAction = 3;

    // Values of the checkbox and spinner
    private boolean mUseAlg;
    private int mSelectedItem;

    /**
     * Setup of the views and their listeners
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segmentation);

        // Get input image URI and the parameters
        Intent intent = getIntent();
        mInputImage = intent.getData();
        mUseAlg = intent.getBooleanExtra(MainActivity.EXTRA_USE_ALG, false);
        mSelectedItem = intent.getIntExtra(MainActivity.EXTRA_SELECTED_ITEM, 0);

        mProgressBar = findViewById(R.id.progress_bar);

        mBlurBar = findViewById(R.id.blur_controller);
        mBlurBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) { }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Upon release, update the blur and display
                new BlurringTask().execute(sBlurAction, seekBar.getProgress(), mErosionBar.getProgress(), mBlurGraduationBar.getProgress());
            }
        });

        mBlurGraduationBar = findViewById(R.id.blur_graduation_controller);
        mBlurGraduationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) { }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Upon release, update the blur and display
                new BlurringTask().execute(sBlurringGraduationAction, mBlurBar.getProgress(), mErosionBar.getProgress(), seekBar.getProgress());
            }
        });

        mErosionBar = findViewById(R.id.erosion_controller);
        mErosionBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) { }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Upon release, update the erosion/dilation and display
                new BlurringTask().execute(sMorphologyAction, mBlurBar.getProgress(), seekBar.getProgress(), mBlurGraduationBar.getProgress());
            }
        });

        // Show image
        mPreviewImage = findViewById(R.id.previewView);
        mPreviewImage.setImageURI(mInputImage);

        // Initialize OpenCV
        if (!OpenCVLoader.initDebug()) {
            Toast.makeText(getApplicationContext(), "Cannot load OpenCV", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            // Apply segmentations, blur and update the image
            new BlurringTask().execute(sPipelineAction, mBlurBar.getProgress(), mErosionBar.getProgress(), mBlurGraduationBar.getProgress());
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d("ERROR:", ex.toString());
        }
    }

    /**
     * Invoke the neural network on the input image and retrieve the mask.
     * @param inputImage URI of the input image
     * @throws IOException if the image bitmap cannot be retrieved
     */
    public void neuralNetworkJob(Uri inputImage) throws IOException {

        // Initialize TFLite interface
        DeepLabTFLite tfInterface = new DeepLabTFLite();
        final boolean res = tfInterface.initialize(getApplicationContext());
        // Load the image as a bitmap from the URI
        mInputBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), inputImage);
        // Crop the image according to the neural network
        Bitmap croppedBitmap = ImgUtils.resizeBitmap(mInputBitmap, DeepLabTFLite.getInputSize(), DeepLabTFLite.getInputSize());
        Log.i("SegmentationActivity", "Intial size: height=" + mInputBitmap.getHeight() + " width=" + mInputBitmap.getWidth());
        Log.i("SegmentationActivity", "Cropped size: height=" + croppedBitmap.getHeight() + " width=" + croppedBitmap.getWidth());
        // Segmentation through the neural network
        int[][] deepMask = tfInterface.segment(croppedBitmap);
        Log.i("SegmentationActivity", "DeepLab segmentation done.");
        // Remove any object which does not belong to the "person" class
        deepMask = DeepLabTFLite.getSingleLabelMask(deepMask, DeepLabTFLite.getPersonClass(), 255);
        // Create a copy as a bitmap
        mDeepMaskBitmap = ImgUtils.drawMask(deepMask, Color.rgb(255, 0, 0));

    }

    /**
     * Apply erosion or dilation to the mask increasing or decreasing its area.
     * The parameter is controlled by the associated slider
     * @param kernelMorph Value of the kernel of the morphology operator
     */
    public void morphologyMask(int kernelMorph) {
        // No changes to be made
        if (kernelMorph == mFinalMorphValue) return;

        Log.d("morphologyMask", kernelMorph + " kernel size");
        mFinalMorphValue = kernelMorph;
        // The shape of the structuring element is rectangular
        int k = (mFinalMorphValue - 51);
        int elementType = Imgproc.CV_SHAPE_RECT;
        int k_abs = Math.abs(k);
        Mat element = Imgproc.getStructuringElement(elementType, new Size(2 * k_abs + 1, 2 * k_abs + 1),
                new Point(k_abs, k_abs));

        if (k > 0)
            Imgproc.dilate(mUpscaledMaskMat, mFinalMaskMat, element);
        else
            Imgproc.erode(mUpscaledMaskMat, mFinalMaskMat, element);

        // Smooth the edges of the mask
        Size ksize = new Size(21, 21);
        Imgproc.GaussianBlur(mFinalMaskMat, mFinalMaskMat, ksize, 0);

        Log.d("morphologyMask", "done.");
        Utils.matToBitmap(mFinalMaskMat, mFinalMask);
    }

    /**
     * Set method for FinalBlurredValue
     * @param kernelValue Value of the kernel of the Gaussian blur
     */
    public void setFinalBlurredValue(int kernelValue) {


        // kernelValue must be odd
        if (kernelValue % 2 == 0) kernelValue += 1;
        mFinalBlurredValue = kernelValue;

    }

    /**
     * Set method for FinalBlurGraduation
     * @param value Value of the slider bar
     */
    public void setFinalBlurGraduation(int value){
        mFinalBlurGraduation = value / (1000.0);
    }

    /**
     * Apply the blur operation
     * @param kernelValue Value of the kernel of the Gaussian blur
     * @return Blurred bitmap
     */
    public Bitmap blurImageGraduate(int kernelValue) {
        if (kernelValue % 2 == 0) kernelValue += 1;

        // Build an array of kernel values as [kV kV-5 kV-10 kV-15 1]
        int[] kk = new int[5];
        for (int i = 1; i < kk.length; i++) {
            int k = kernelValue - 5 * i;
            if (k < 0) k = 1;
            if (k % 2 == 0) k += 1;
            kk[i - 1] = k;
        }
        kk[4] = 1;

        // Create five bitmaps of the original image with different blur intensities
        Mat[] kkMat = new Mat[]{new Mat(), new Mat(), new Mat(), new Mat(), new Mat()};
        Bitmap[] kkBitmap = new Bitmap[]{
                Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888),
                Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888),
                Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888),
                Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888),
                Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888)
        };
        try {
            for (int i = 0; i < kk.length; i++) {
                Imgproc.GaussianBlur(mInputMat, kkMat[i], new Size(kk[i], kk[i]), 0);
                Utils.matToBitmap(kkMat[i], kkBitmap[i]);
            }
        } catch (Exception ex) {
            Log.d("ERROR", ex.toString());
            return mInputBitmap;
        }

        // Distance Transform
        Mat distMat = new Mat();
        // Requires a single channel image
        Imgproc.cvtColor(mFinalMaskMat, distMat, Imgproc.COLOR_BGRA2GRAY);
        Imgproc.distanceTransform(distMat, distMat, Imgproc.CV_DIST_C, 3);
        distMat.convertTo(distMat, CvType.CV_8U);
        // To bitmap
        Bitmap distBitmap = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(distMat, distBitmap);
        // To array of ints
        int[] pixelDist = new int[distBitmap.getWidth() * distBitmap.getHeight()];
        distBitmap.getPixels(pixelDist, 0, distBitmap.getWidth(), 0, 0, distBitmap.getWidth(), distBitmap.getHeight());
        int[] inputInt = new int[mInputBitmap.getWidth() * mInputBitmap.getHeight()];
        mInputBitmap.getPixels(inputInt, 0, mInputBitmap.getWidth(), 0, 0, mInputBitmap.getWidth(), mInputBitmap.getHeight());
        // Get max and min value
        int max = pixelDist[0], min = pixelDist[0];
        for (int i = 1; i < pixelDist.length; i++) {
            if (pixelDist[i] > max) max = pixelDist[i];
            if (pixelDist[i] < min) min = pixelDist[i];
        }
        // Add offset so the min value is zero
        for (int i = 0; i < pixelDist.length; i++) pixelDist[i] += (-min);
        max += (-min);
        min = 0;

        // Initialize the array which defines a "slice" of the mask
        int[] outer = new int[pixelDist.length];
        for (int i = 0; i < outer.length; i++) outer[i] = pixelDist[i];

        final double THRESHOLD = 0.025;
        double high = THRESHOLD, low = THRESHOLD;
        // For each Gaussian kernel (blurred image)
        for (int i = 0; i < kk.length; i++) {
            // Get i-th blurred version of the image
            int[] blur = new int[pixelDist.length];
            kkBitmap[i].getPixels(blur, 0, mInputBitmap.getWidth(), 0, 0, mInputBitmap.getWidth(), mInputBitmap.getHeight());
            // The first blur (which has highest kernel) is applied to the background
            if (i == 0) {
                // Put the blurred pixels of the current blurred image in the output
                // If the distance transform in not zero in that pixel (it would be the same as using the original mask)
                for (int p = 0; p < pixelDist.length; p++) {
                    if (outer[p] == min) inputInt[p] = blur[p];
                }
                // The first "slice" is the distance transform with distances greater than THRESHOLD * MAX_DISTANCE set to zero
                for (int q = 0; q < outer.length; q++)
                    if (outer[q] > max * high) outer[q] = min;
                // Update the threshold
                high += mFinalBlurGraduation;
            } else {
                // Put the blurred pixels of the current blurred image in the output
                // If the slice is not zero in that pixel
                for (int p = 0; p < pixelDist.length; p++) {
                    if (outer[p] > min) inputInt[p] = blur[p];
                }
                // The next "slice" is the distance transform with distances greater than HIGH * MAX_DISTANCE and
                // distances smaller than LOW * MAX_DISTANCE equal to zero
                for (int q = 0; q < outer.length; q++) {
                    outer[q] = pixelDist[q];
                    if (outer[q] <= max * low) outer[q] = min;
                    if (outer[q] > max * high) outer[q] = min;
                }
                // Update the thresholds
                low = high;
                high += mFinalBlurGraduation;
            }
        }
        return Bitmap.createBitmap(inputInt, mInputBitmap.getWidth(), mInputBitmap.getHeight(), Bitmap.Config.ARGB_8888);

    }

    /**
     * Initializes Bitmap and Mat resources, resizes the mask from the Neural Network to the original dimension of the image
     */
    public void initBitmapAndMat() {


        mInputMat = new Mat();
        Utils.bitmapToMat(mInputBitmap, mInputMat);

        Mat croppedDeepMaskMat = new Mat();
        Utils.bitmapToMat(mDeepMaskBitmap, croppedDeepMaskMat);

        mUpscaledMaskMat = new Mat();
        mFinalMaskMat = new Mat();
        Size sz = new Size(mInputBitmap.getWidth(), mInputBitmap.getHeight());
        Imgproc.resize(croppedDeepMaskMat, mUpscaledMaskMat, sz);
        Imgproc.resize(croppedDeepMaskMat, mFinalMaskMat, sz);

        mUpscaledMask = Bitmap.createBitmap(mUpscaledMaskMat.cols(), mUpscaledMaskMat.rows(), Bitmap.Config.ARGB_8888);
        mFinalMask = Bitmap.createBitmap(mUpscaledMaskMat.cols(), mUpscaledMaskMat.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(mUpscaledMaskMat, mUpscaledMask);
        Utils.matToBitmap(mUpscaledMaskMat, mFinalMask);

    }

    // Number of concurrent threads used for segmentation
    private int mNumTh = 0;
    // Enforces the wait() if the first thread is not ready
    private boolean mGoOn = false;

    /**
     * The segmentation algorithm requires a large amount of resources for computation.
     * To reduce the execution time the input image is rescaled to a 1/4 of its original size.
     * The resulting image is then divided in blocks of max 250x250 each and each block is processed on a separated thread.
     * A maximum of 5 concurrent threads at time are allowed for execution.
     */
    public void segmentationAlgorithm() {


        //Copy Bitmap and Mat of the original image
        Mat inputMatCopy = new Mat();
        Utils.bitmapToMat(mInputBitmap, inputMatCopy);
        Bitmap inputCopy = Bitmap.createBitmap(inputMatCopy.cols(), inputMatCopy.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(inputMatCopy, inputCopy);

        //Reduce input image to 1/4
        Mat red = new Mat();
        Imgproc.resize(mInputMat, red, new Size(mInputBitmap.getWidth() / 4, mInputBitmap.getHeight() / 4));
        mInputMat = red;
        mInputBitmap = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mInputMat, mInputBitmap);

        //Reduce mask to 1/4
        red = new Mat();
        Imgproc.resize(mUpscaledMaskMat, red, new Size(mInputMat.cols(), mInputMat.rows()));
        mUpscaledMaskMat = red;
        mUpscaledMask = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);
        mFinalMask = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mUpscaledMaskMat, mUpscaledMask);
        Utils.matToBitmap(mUpscaledMaskMat, mFinalMask);

        // Convert to Lab color space for better algorithm performance
        Imgproc.cvtColor(mInputMat, mInputMat, Imgproc.COLOR_BGR2Lab);
        int[] pixelMask = new int[mInputMat.rows() * mInputMat.cols()];
        int numRows = mInputMat.rows();
        int numCols = mInputMat.cols();
        mFinalMask.getPixels(pixelMask, 0, mFinalMask.getWidth(), 0, 0, mFinalMask.getWidth(), mFinalMask.getHeight());

        // Start the recursive algorithm
        segmentationAlgorithmRec(pixelMask, numRows, numCols, 0, numRows - 1, 0, numCols - 1);

        synchronized (this) {
            // Wait for all thread to finish and the main thread to be ready
            while (!mGoOn || mNumTh > 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // Update the Upscaled Mask after the merging of the blocks
        mUpscaledMask = Bitmap.createBitmap(pixelMask, numCols, numRows, Bitmap.Config.ARGB_8888);
        Utils.bitmapToMat(mUpscaledMask, mUpscaledMaskMat);

        // Restore the input image
        mInputBitmap = inputCopy;
        mInputMat = inputMatCopy;

        // Rescale the mask after the segmentation algorithm
        Imgproc.resize(mUpscaledMaskMat, mUpscaledMaskMat, new Size(mInputMat.cols(), mInputMat.rows()));
        mUpscaledMask = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);
        mFinalMask = Bitmap.createBitmap(mInputMat.cols(), mInputMat.rows(), Bitmap.Config.ARGB_8888);

        // Smooth the edges
        Size ksize = new Size(21, 21);
        Imgproc.GaussianBlur(mUpscaledMaskMat, mUpscaledMaskMat, ksize, 0);
        Utils.matToBitmap(mUpscaledMaskMat, mUpscaledMask);
        Utils.matToBitmap(mUpscaledMaskMat, mFinalMask);
        Utils.bitmapToMat(mFinalMask, mFinalMaskMat);

    }

    /**
     * Increase the count of running threads. Waits if the limits of 5 threads is reached.
     */
    public synchronized void incrTh() {
        while (mNumTh > 4) try {
            wait();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mNumTh++;
    }

    /**
     * Decrease the count of running threads and notifies any waiting threads.
     */
    public synchronized void decrTh() {
        mNumTh--;
        notifyAll();
    }

    /**
     * Apply the subdivision of the image in blocks recursively and assign each block to a thread to perform the segmentation
     * @param pixelMask integer array of the segmentation mask
     * @param numRows   height of the mask
     * @param numCols   width of the mask
     * @param rowStart  starting height of the block
     * @param rowEnd    final height of the block
     * @param colStart  starting width of the block
     * @param colEnd    final width of the block
     */
    public void segmentationAlgorithmRec(int[] pixelMask, int numRows, int numCols, int rowStart, int rowEnd, int colStart, int colEnd) {
        try {
            // If one dimension is greater than 250px subdivide the image in 4 equal blocks and reiterate
            if (rowEnd - rowStart + 1 > 250 || colEnd - colStart + 1 > 250) {
                int middleRow = (rowStart + rowEnd) / 2;
                int middleCol = (colStart + colEnd) / 2;
                segmentationAlgorithmRec(pixelMask, numRows, numCols, rowStart, middleRow, colStart, middleCol);
                segmentationAlgorithmRec(pixelMask, numRows, numCols, middleRow + 1, rowEnd, colStart, middleCol);
                segmentationAlgorithmRec(pixelMask, numRows, numCols, rowStart, middleRow, middleCol + 1, colEnd);
                segmentationAlgorithmRec(pixelMask, numRows, numCols, middleRow + 1, rowEnd, middleCol + 1, colEnd);
                return;
            }

            // If both dimensions are less than 250px then start the thread to apply the segmentation
            this.new SegmentationAlgorithmTh(pixelMask, numRows, numCols, rowStart, rowEnd, colStart, colEnd).start();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * For each superpixel the method counts the number of pixel that also belong to the mask from the output of the neural network.
     * If less than 30% of the pixels fall inside of the mask then all pixels of the superpixel are removed from the mask.
     * If more than 70% of the pixels fall inside of the mask then all pixels of the superpixel are added to the mask.
     * @param pixelMask   integer array of the segmentation mask
     * @param numRows     height of the mask
     * @param numCols     width of the mask
     * @param superPixels an ArrayList containing all the superpixels obtained from the block of image
     */
    public void mergeMaskSuperpixel(int[] pixelMask, int numRows, int numCols, ArrayList[] superPixels) {

        // For each superpixel
        for (int i = 0; i < superPixels.length; i++) {
            double threshold = 0.0;
            // Get the pixels associated to the superpixel
            ArrayList<Pair<Integer, Integer>> pixels = superPixels[i];
            for (int j = 0; j < pixels.size(); j++) {
                int row = pixels.get(j).first;
                int col = pixels.get(j).second;
                // If the pixel is non zero in the mask increase the count
                if (pixelMask[numCols * (numRows - row - 1) + col] == Color.rgb(255, 0, 0)) {
                    threshold += 1.0;
                }
            }
            // Computer the fraction
            threshold = threshold / pixels.size();
            Log.d("THRESHOLD", "" + threshold);
            // Set to transparent the pixels if the threshold is not high enough
            if (threshold > 0.0 && threshold < 0.3) {
                for (int j = 0; j < pixels.size(); j++) {
                    int row = pixels.get(j).first;
                    int col = pixels.get(j).second;
                    pixelMask[numCols * (numRows - row - 1) + col] = Color.TRANSPARENT;
                }
            }
            // Set to non-transparent the pixels if the threshold is high enough
            if (threshold < 1.0 && threshold >= 0.7) {
                for (int j = 0; j < pixels.size(); j++) {
                    int row = pixels.get(j).first;
                    int col = pixels.get(j).second;
                    pixelMask[numCols * (numRows - row - 1) + col] = Color.rgb(255, 0, 0);
                }
            }
        }


    }


    /**
     * Apply blur and update the preview
     * @param preview view of the activity where to draw the mOutputBitmap
     */
    public void applyMask(ImageView preview) {

        mOutputBitmap = blurImageGraduate(mFinalBlurredValue);
        preview.setImageBitmap(mOutputBitmap);

    }

    /**
     * Check if the external storage is available for writing operations
     * @return true if the storage is available
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }


    private final int MEDIA_IMG = 1;

    /**
     * Listener of the button for saving the image
     * @param view
     */
    public void saveImage(View view) {
        new SaveImage().execute();
    }

    /**
     * Create a File for saving an image or video
     * @param type Type of media to store (image, video)
     * @return      File stream containing the altered photo
     */
    private  File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PortraitOfDG");

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_IMG){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    /**
     * Implements the pipeline for the execution of the task in background
     */
    private class BlurringTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // While working disable the bars and show the progress bar
            mBlurBar.setEnabled(false);
            mErosionBar.setEnabled(false);
            mBlurGraduationBar.setEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            try {
                switch (integers[0]) { // the first parameter signal where the call is from

                    case sPipelineAction:
                        // Segmentation with neural network
                        neuralNetworkJob(mInputImage);
                        // Bitmap and Mat initializations and resizing
                        initBitmapAndMat();
                        // Apply segmentation algorithm
                        if (mUseAlg) segmentationAlgorithm();
                        // Set blur values
                        setFinalBlurredValue(integers[1]);
                        setFinalBlurGraduation(integers[3]);
                        // Morphology operations
                        morphologyMask(integers[2]);
                        break;
                    case sBlurAction:
                        // Set blur values
                        setFinalBlurredValue(integers[1]);
                        break;
                    case sMorphologyAction:
                        // Change morphology operations
                        morphologyMask(integers[2]);
                        break;
                    case sBlurringGraduationAction:
                        // Set gradient blur values
                        setFinalBlurGraduation(integers[3]);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            // Apply the blur operations and display the image
            applyMask(mPreviewImage);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Re-enable the bars and hide the progress bar
            mBlurBar.setEnabled(true);
            mErosionBar.setEnabled(true);
            mBlurGraduationBar.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Thread for the segmentation algorithm
     */
    private class SegmentationAlgorithmTh extends Thread {

        // Internal values
        private int[] pixelMask;
        private int numRows;
        private int numCols;
        private int rowStart;
        private int rowEnd;
        private int colStart;
        private int colEnd;

        /***
         * Initializes the segmentation algorithm thread
         * @param pixelMask integer array of the segmentation mask
         * @param numRows   height of the mask
         * @param numCols   width of the mask
         * @param rowStart  starting height of the block
         * @param rowEnd    final height of the block
         * @param colStart  starting width of the block
         * @param colEnd    final width of the block
         */
        public SegmentationAlgorithmTh(int[] pixelMask, int numRows, int numCols, int rowStart, int rowEnd, int colStart, int colEnd) {
            this.pixelMask = pixelMask;
            this.numRows = numRows;
            this.numCols = numCols;
            this.rowStart = rowStart;
            this.rowEnd = rowEnd;
            this.colStart = colStart;
            this.colEnd = colEnd;
        }

        @Override
        public void run() {
            // Increment thread number
            incrTh();
            mGoOn = true;
            // Ratio parameter for LSC
            final float ratio = (float) 0.075;
            // Extract the portion denoted by row/col-Start/End
            Mat portion = mInputMat.rowRange(rowStart, rowEnd);
            portion = portion.colRange(colStart, colEnd);
            // Initialize label Mat for superpixels
            Mat labels = new Mat();

            int numberOfSp = 0;
            // Use the selection of the slider
            switch (mSelectedItem) {
                case 0:
                    SuperpixelLSC lsc = Ximgproc.createSuperpixelLSC(portion, 5, ratio);
                    lsc.iterate();
                    lsc.getLabels(labels);
                    numberOfSp = lsc.getNumberOfSuperpixels();
                    break;
                case 1:
                    SuperpixelSLIC slic = Ximgproc.createSuperpixelSLIC(portion, Ximgproc.MSLIC, 5);
                    slic.iterate();
                    slic.getLabels(labels);
                    numberOfSp = slic.getNumberOfSuperpixels();
                    break;
            }

            ArrayList[] superPixels = new ArrayList[numberOfSp];

            // Superpixel are labelled from zero to numberOfSp-1
            // For each one we create a list of the pixels and put it in superPixels
            for (int i = 0; i < numberOfSp; i++)
                superPixels[i] = new ArrayList<Pair<Integer, Integer>>();
            int cc = 1;
            for (int y = 0; y < portion.rows(); y++) {
                for (int x = 0; x < portion.cols(); x++) {
                    int index = (int) labels.get(y, x)[0];
                    superPixels[index].add(new Pair<Integer, Integer>(rowStart + y, colStart + x));
                }
            }
            // Compare the superpixels with the mask
            mergeMaskSuperpixel(pixelMask, numRows, numCols, superPixels);
            // Decrease the number of threads
            decrTh();
        }


    }

    /**
     * Task to save the final image
     */
    private class SaveImage extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            // If no image is in output
            if(mOutputBitmap == null) return null;
            // If external storage is not writable
            if(!isExternalStorageWritable()) return null;
            // Create directory and file
            File pictureFile = getOutputMediaFile(MEDIA_IMG);
            if (pictureFile == null){
                return null;
            }

            try {
                // Write file and close it
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(ImgUtils.bitmapToByteArray(mOutputBitmap));
                fos.close();
            } catch (Exception e) {
                Log.d("ERRORE", "File not found: " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Show a toast if successful
            Toast.makeText(getApplicationContext(), "Image Saved", Toast.LENGTH_LONG).show();
        }
    }
}
